---
title: "Markdown copie"
date: "2021-07-20T11:53:10+02:00"
keywords: []

weight: 0

categories: []
tags: []

draft: true
---
# h1 Heading
## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading


## CommonMark native

### Text

Phasellus posuere non turpis at aliquet. Vestibulum ullamcorper nunc nisi, quis facilisis urna rutrum ac. Nulla tempor justo at ligula egestas, eget interdum libero rhoncus. Cras egestas vitae mauris id placerat. Pellentesque rhoncus arcu a neque sodales, eget aliquet urna interdum. Quisque eleifend, felis at ornare pellentesque, dolor magna ullamcorper urna, sed varius lectus justo in erat. Nullam vitae ornare massa.

Nulla condimentum libero vel enim placerat, porta dignissim velit consequat. Nullam rutrum hendrerit ultrices. Aliquam lacinia lobortis ex, ut tristique mi iaculis a. Quisque molestie ultricies justo, at porttitor lacus commodo quis. Phasellus libero eros, lobortis at mauris vitae, scelerisque auctor velit. Maecenas commodo, ligula id ultrices rhoncus, eros nunc iaculis dui, eget cursus nibh sapien sit amet nisi.


### Emphasis

**This is bold text**

__This is bold text__

*This is italic text*

_This is italic text_

~~Strikethrough~~


### Links

[Link](https://www.raideus.fyi/)

[LinkWithTag][link1]

[link1]: https://www.raideus.fyi/

Auto links (see: https://github.github.com/gfm/#autolinks-extension-)

https://www.raideus.fyi/

www.raideus.fyi


### Images

![Minion](https://octodex.github.com/images/minion.png)
![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")

Like links, Images also have a footnote style syntax

![Alt text][id_img]

With a reference later in the document defining the URL location:

[id_img]: https://octodex.github.com/images/dojocat.jpg  "The Dojocat"

{{%
  figure 
  src="https://octodex.github.com/images/minion.png"
  title="Ceci est un titre pour l'image"
  caption="This is a caption for the figure"
%}}


### Blockquotes

> This is a blockquote

> This is another one

> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.


### Lists

Unordered

Nulla condimentum libero vel enim placerat, porta dignissim velit consequat. Nullam rutrum hendrerit ultrices. Aliquam lacinia lobortis ex, ut tristique mi iaculis a. Quisque molestie ultricies justo, at porttitor lacus commodo quis. Phasellus libero eros, lobortis at mauris vitae, scelerisque auctor velit. Maecenas commodo, ligula id ultrices rhoncus, eros nunc iaculis dui, eget cursus nibh sapien sit amet nisi.

This is a loose list

* Create a list by starting a line with `-`, or `*`

* Sub-lists are made by indenting 2 spaces:

  * Marker character change forces new list start:

    * Ac tristique libero volutpat at c'est une longue ligne une longue ligne une longue ligne une longue ligne une longue ligne une longue ligne une longue ligne

    * Facilisis in pretium nisl aliquet

    * Nulla volutpat aliquam velit

* Very easy!

Ordered

This is a tight list

1. Lorem ipsum dolor sit amet 
1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`

Start numbering with offset:

57. foo
1. bar

* Test panda
* Test Panda 1
* Test Panda 2

- Test Panda
  - Test Panda 1
  - Test panda 2

### Horizontal Rules

---

***


### Code

Inline `code`

Block code "fences"

```
Sample text here...
```

```
$ hugo --verbose
INFO: 2014/09/29 Using config file: config.toml
INFO: 2014/09/29 syncing from /Users/quoha/Sites/zafta/static/ to /Users/quoha/Sites/zafta/public/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
WARN: 2014/09/29 Unable to locate layout: [index.html _default/list.html _default/single.html]
WARN: 2014/09/29 Unable to locate layout: [404.html]
0 draft content
0 future content
0 pages created
0 tags created
0 categories created
```

Syntax highlighting with markdown

``` js
var foo = function (bar) {
    return bar++;
};

console.log(foo(5));
```

Syntax highlighting with shortcode

{{< code js >}}
var foo = function (bar) {
    return bar++;
};

console.log(foo(5));
{{< /code >}}

{{< code js "2-3" >}}
var foo = function (bar) {
    console.log("oui");
    return bar++;
};

console.log(foo(5));
{{< /code >}}


## Goldmark extensions

### Tables

See: https://github.github.com/gfm/#tables-extension-

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

Right aligned columns

| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |


### Strikethrough

See: https://github.github.com/gfm/#strikethrough-extension-

~~Deleted text~~


### Task list

See: https://github.github.com/gfm/#task-list-items-extension-

- [ ] Todo
- [x] Done


### Definition list

See: https://michelf.ca/projects/php-markdown/extra/#def-list

Term 1
: Definition 1
  with lazy continuation.

Term 1 too
Term 2 with *inline markup* and multiple paragraphs
: Definition 2

  Third paragraph of definition 2.

  ```
  Help
  ```

  Fourth paragraph


### Footnotes

See: https://michelf.ca/projects/php-markdown/extra/#footnotes

Footnote 1 link[^first].

Footnote 2 link[^second].

Duplicated footnote reference[^second].

[^first]: Footnote **can have markup**

    and multiple paragraphs.

[^second]: Footnote text.


### Typographer

"Cool apostrophes"

Cool -- double-dash

Cool --- triple-dash

Ellipsis character...