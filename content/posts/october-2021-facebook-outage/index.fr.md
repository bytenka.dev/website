---
title: "Panne Facebook d'octobre 2021"
date: "2021-10-07T13:12:14+02:00"
keywords: []

weight: 0

categories: [ "whatsdis" ]
tags: [ "internet", "routage", "facebook", "cloudflare", "dns", "bgp" ]

draft: true
---
# La panne Facebook d'octobre 2021, whatsdis&nbsp;?

> Cet article est largement inspiré du [billet de blog](https://blog.cloudflare.com/october-2021-facebook-outage/) mis en ligne par CloudFlare, et vise à en proposer une version française plus accessible pour les non-initiés.

Le 4 octobre 2021, aux alentours de 15h40 UTC (17h40 heure française), presque tous les services liés de près ou de loin à Facebook sont devenus inaccessibles. Messenger, Instagram, WhatsApp, même Oculus, tous ont disparu pendant quelques heures d'internet sans laisser de traces, si ce n'est un torrent de tweets moqueurs qui a rapidement fait vaciller les serveurs de Twitter.

Le coupable ? Une erreur de configuration, déployée automatiquement sur un large nombre d'équipements, et à un effet boule de neige mondial à la hauteur de l'omnipotence de l'entreprise. Du moins, ce sont les seules informations qu'on peut tirer du [communiqué officiel](https://engineering.fb.com/2021/10/04/networking-traffic/outage/), bien plus acharné à présenter des excuses quant aux conséquences plutôt qu'à expliquer les causes techniques du problème.

Un [deuxième document](https://engineering.fb.com/2021/10/05/networking-traffic/outage-details/) publié le lendemain revient plus en détails sur l'incident, et vient confirmer les informations qu'un utilisateur Reddit du pseudonyme de "u/ramenporn", membre de l'équipe d'urgence de réponse à incident chez Facebook, nous avait déjà communiqué afin de comprendre un peu mieux ce qu'il s'est réellement passé. Vous avez sûrement déjà rencontré des termes comme DNS ou BGP à l'occasion de la réaction à la panne sur les réseaux sociaux, mais leur signification vous échappe peut-être. Dans cet article, nous allons essayer d'expliquer le plus simplement possible ce qui s'est passé ce jour-là et quelles conséquences nous pouvons en tirer.


## Rappel du fonctionnement d'internet

Internet est un réseau, c'est-à-dire un ensemble de machines qui sont connectés entre elles et qui peuvent s'envoyer des messages. Dans le jargon, ces messages sont appelés des paquets. Plutôt que de connecter chaque machine avec toutes les autres machines à l'aide d'un câble dédié, le réseau transmet les informations de proche en proche du point de départ vers le point d'arrivée, à la manière du système postal. Par exemple pour contacter Facebook, plutôt que de vous déplacer directement au siège de l'entreprise pour déposer votre lettre, vous utilisez un service postal qui, de centre de tri en centre de tri, achemine votre lettre jusqu'à Facebook pour vous.


### Systèmes autonomes

Dans cette analogie, les centres de tri sont des routeurs. Ceux-ci sont contrôlés par une entreprise (LaPoste, DHL, FedEx, etc.) qui gère en interne l'organisation des centres et construit des routes entre un centre et un autre. Les entreprises s'apparentent donc ici à ce qu'on appelle des *systèmes autonomes* (AS pour *autonomous system* en anglais). Ils portent bien leur nom, puisque toute la gestion et l'organisation du service au sein d'un *système autonome* est réalisée de manière autonome, sans dépendance ni influence extérieure. En somme, c'est un réseau fermé au monde extérieur.

Une des particularités d'internet, du moins encore aujourd'hui, est de pouvoir envoyer et recevoir des paquets vers et depuis le monde entier. N'étant pas présents à l'échelle mondiale, les *systèmes autonomes* qui le composent doivent s'accorder entre eux pour qu'il existe toujours un chemin d'un point à l'autre du réseau, quels qu'ils soient. Un *système autonome* accepte alors de prendre en charge les paquets de ses voisins, soit pour les livrer si la destination est à l'intérieur de ses frontières, soit pour transporter à un autre *système autonome* plus proche de la destination. Vous avez peut-être entendu dire qu'internet est un réseau de réseaux, c'est le sens de cet adage.


### BGP

Les routeurs aux frontières des *systèmes autonomes* doivent donc gérer des messages qui ne circulent qu'à l'intérieur, mais également d'autres en provenance ou à destination de leurs voisins en dehors de leurs frontières. Ils sont notamment responsables d'informer ces routeurs étrangers que, si un paquet est à destination d'un appareil à l'intérieur de son *système autonome*, ils peuvent servir d'étape et le prendre en charge. C'est comme ça qu'internet parvient à interconnecter n'importe quel appareil avec n'importe quel autre. Pour se mettre d'accord et échanger ces informations, les routeurs parlent tous la même langue&nbsp;: BGP.

BGP est un protocole, c'est-à-dire une langue avec un dictionnaire très précis qui permet à tous ceux qui la parlent de se comprendre sans ambiguïté. On distingue deux versions de ce protocole&nbsp;: eBGP et iBGP. Le premier, eBGP, est celui employé pour les discussions avec les autres routeurs à l'extérieur du *système autonome*, et le second est celui employé pour toutes les communications internes au *système autonome*. Pour pousser les analogies avec le langage, eBPG utilise un vocabulaire soutenu et diplomatique pour donner des indications aux routeurs étrangers, et iBGP utilise un vocabulaire plus familier pour décrire les directions à prendre à l'intérieur du *système autonome*.


### Tables de routage

Tous les routeurs entretiennent localement une carte géographique qui leur permet de savoir quel sont les routes praticables pour atteindre un voisin, ainsi que les destinations que ces voisins permettent d'atteindre. Il n'existe pas de carte centralisée d'internet, mais chaque routeur dispose d'une vision globale du monde grâce à son propre voisinage. Plutôt que de connaître toutes les routes possibles pour se rendre à n'importe quelle destination, chaque routeur sait uniquement que pour telle destination, le plus rapide est de passer par tel voisin. Un paquet fait donc étape à chaque routeur sans connaître le chemin exact à emprunter, mais se déplacer comme ça de voisin en voisin revient en fait absolument au même tant qu'on ne tourne pas en rond.

Dans le jargon, ces cartes géographiques locales s'appellent des *tables de routage* (*routing table* en anglais), et on parle également de route pour désigner une route empruntable entre un routeur et un autre. Tous les routeurs prennent grand soin de maintenir leur propre *table de routage* bien à jour pour éviter d'envoyer des paquets sur des routes impraticables ou bloquées.


### DNS

Les paquets envoyés sur le réseau comportent une destination vers laquelle ils doivent être acheminés. Vous le savez sûrement, une adresse internet s'appelle une *adresse IP* et consiste en une suite difficile à retenir de chiffres et parfois de lettres. Ces adresses sont pour nous [humains](https://howhumanareyou.com/) difficiles à retenir, et internet étant un réseau très dynamique, celles-ci peuvent changer régulièrement. Le *service de noms de domaine* (DNS pour *Domain Name Service*) vise à régler ce problème en proposant un annuaire associant des *noms de domaine* à des *adresses IP*. Vous êtes sûrement déjà familiers des *noms de domaine*, `facebook.com` en est un par exemple. Votre ordinateur sait que pour contacter `facebook.com`, il doit transformer ce nom en une *adresse IP*, et il fait cela en demandant à un *serveur DNS* de le faire pour lui.


## La panne de Facebook

Facebook est lui-même un *système autonome* qui doit gérer de nombreux routeurs pour permettre l'accès aux différents services. Ces services étant disponibles dans de nombreux pays, il leur est nécessaire de disposer de plusieurs serveurs géographiquement proches de leurs clients afin d'assurer un service de qualité. Ils doivent également interconnecter ces serveurs entre eux pour qu'ils puissent échanger des informations rapidement.


### "It's always DNS"

Le jour de la coupure, les *systèmes autonomes* voisins comme [celui de CloudFlare](https://twitter.com/jgrahamc/status/1445068309288951820) ont détecté une forte activité eBGP qui a conduit à marquer de nombreuses routes à destination de Facebook comme impraticables. Une opération de maintenance réalisée à ce moment-là chez Facebook a, pour des raisons qui restent encore floues, coupé une partie du réseau interne de l'entreprise. D'après le communiqué officiel, un enchaînement de coupures automatiques au départ prévues pour préserver l'infrastructure s'est mise marche, rendant ainsi impossible l'accès aux *serveurs DNS* situés à l'intérieur depuis l'extérieur. Les routeurs à la frontière avaient en effet reçu l'ordre de marquer toutes les routes impraticables, rendant la communication interne et avec l'extérieur impossible.

Une coupure d'une telle ampleur signifie malheureusement que le problème ne peut pas être réglé rapidement à distance. Il faut en effet se déplacer et intervenir directement sur certains équipements, et cela demande du temps. Pendant que l'équipe de réponse à incident passait les vérifications de sécurité pour avoir accès aux équipements, d'autres employés étaient [bloqués à l'extérieur des bâtiments](https://twitter.com/MikeDoughney/status/1445089349826224131), car leurs badges d'accès, dépendants du réseau désormais coupé, ne fonctionnaient plus.


### Conséquences

Facebook est omniprésent sur internet. De nombreux sites utilisent leurs services, que ce soit pour la gestion des publicités ou l'authentification des utilisateurs. De nombreuses entreprises dépendent de Facebook pour trouver leurs clients. Les répercussions ont été ressenties bien au-delà de Facebook eux-mêmes, et tant sur le plan économique que technique puisque certains sites dépendants ont été rendus inaccessibles.

Mais un autre problème à rapidement commencé à affecter tous les utilisateurs d'internet&nbsp;: l'augmentation brutale du trafic. En effet, avec la combinaison d'utilisateurs mécontents qui tentaient à répétition d'accéder aux services, et les appareils qui périodiquement retentent d'eux-mêmes de rétablir la connexion, le trafic sur le réseau a augmenté et les équipements réseau ont été mis à rude épreuve. Les autres *serveurs DNS* ont eu du mal à suivre la cadence et on vu [leur temps de réponse fortement augmenter](https://twitter.com/RDSitze/status/1445112742461931525). Les noms de domaine comme `facebook.com` étant enregistrés, mais les serveurs étant injoignables, de nombreuses connexions n'aboutissaient pas et restait en attente, monopolisant au passage des ressources de calcul rendues indisponibles pour les autres utilisateurs. Les serveurs de Twitter ont également eu du mal à suivre, bien que le réseau social [n'a pas manqué d'ironiser](https://twitter.com/Twitter/status/1445078208190291973) sur la situation.

Il aura fallu plus de 5 heures avant que le problème ne soit résolu, mais le hashtag Twitter [#FacebookDown](https://twitter.com/search?q=until%3A2021-10-06%20since%3A2021-10-04%20%23facebookdown) est resté plusieurs jours en tendance, et les conséquences sociales ont également été importantes. Le constat qu'internet, du moins l'aspect le plus populaire, pouvait être mis en difficulté par l'indisponibilité d'une seule de ses parties, est un problème qui a fait couler beaucoup d'encre parmi ceux qui restent attachés à la philosophie originelle du réseau&nbsp;: la décentralisation. Internet ne se limite pas qu'aux grandes entreprises qui chassent dessus, et même si de telles pannes restent rarissimes, ce genre d'événements nous permet de reprendre conscience de l'ordre de grandeur que des entreprise comme Facebook ont atteint en une dizaine d'années.
