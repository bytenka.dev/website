---
title: "Generating GRUB passwords with OpenSSL 3.0"
date: "2023-11-11T12:54:00+01:00"
keywords: []

weight: 0

categories: [ linux, sysadmin, grub ]
tags: [ grub, password, passphrase, openssl, bash ]

draft: false
---
# Generating GRUB passwords with OpenSSL 3.0

Setting a password for GRUB is a common and useful security measure to block users from editing the boot configuration, and prevent them from gaining root access on a locked system.

To set a password, one must usually use `grub-mkpasswd-pbkdf2` to get generate a key from a passphrase, and add this key to the GRUB configuration. The process varies between distributions, and whether GRUB is enabled for EFI or not.

However, `grub-mkpasswd-pbkdf2` may not be sufficient for some use cases, as it is a very rudimentary tool. For example, it is not possible to use a fixed salt, and a random one will be selected for each call. This makes it hard to integrate with systems like Ansible, which care about not changing the system if not required.

GRUB generates its **64** bytes keys using [PBKDF2](https://en.wikipedia.org/wiki/PBKDF2), with **10000** rounds and a **SHA512** digest. For systems with OpenSSL 3.0 available, it is possible to use the `openssl kdf` subsystem to emulate what `grub-mkpasswd-pbkdf2` does, and be more flexible. The following Bash script generates a GRUB-compatible key string using a custom passphrase and salt. If no salt is provided, a random one will be used.

``` bash
#!/usr/bin/bash

# GRUB format contains the salt and the key separated by a dot (.).
# Bytes are encoded in uppercase hexadecimal form. The salt is decoded before use.
# "openssl kdf" supports loading the salt in hexadecimal form, which makes handling
# non-ascii data way easier for us.

readonly pass="my_password"
readonly salt="custom_salt"

readonly opt_digest="sha512"
readonly opt_iter="10000"

declare salt_hex=""
if [[ -z "${salt:-}" ]]; then
    # No salt provided, use a random one.
    salt_hex="$(openssl rand -hex 64)"
else
    # Salt provided, make it 64 bytes long (repeat or cut as needed).
    declare -i salt_length=${#salt}
    for i in {0..63}; do
        salt_hex="${salt_hex}$(cut -c $(( (i % salt_length) + 1 )) <<< "${salt}")"
    done

    # Convert to hex.
    salt_hex="$(echo -n "${salt_hex}" | xxd -plain -cols 0 -upper)"
fi

pass_hex="$(echo -n "${pass}" | xxd -plain -cols 0 -upper)"
pass_key="$(openssl kdf -keylen 64 -kdfopt "digest:${opt_digest}" -kdfopt "iter:${opt_iter}" -kdfopt "hexpass:${pass_hex}" -kdfopt "hexsalt:${salt_hex}" PBKDF2 | tr -d ':')"

printf "grub.pbkdf2.%s.%s.%s.%s" "${opt_digest}" "${opt_iter}" "${salt_hex}" "${pass_key}"
```

The output of this script will look something like this:

``` bash
grub.pbkdf2.sha512.10000.637573746F6D5F73616C74637573746F6D5F73616C74637573746F6D5F73616C74637573746F6D5F73616C74637573746F6D5F73616C74637573746F6D5F7361.15255E753F544B00048D36495F900B900589DED876D62758DF8592C333B62F696E8B760442126933744A6630E55C9A0D68C451F6E2845D557BB6662359B4B2E4
```

This can then be used to set a GRUB password. For example, on distributions where `/etc/grub.d/*` scripts are configured to source a `/boot/grub/custom.cfg` file:

``` bash
# file: /boot/grub/custom.cfg

set superusers="root"
password root grub.pbkdf2.sha512.10000.637573746F6D5F73616C74637573746F6D5F73616C74637573746F6D5F73616C74637573746F6D5F73616C74637573746F6D5F73616C74637573746F6D5F7361.15255E753F544B00048D36495F900B900589DED876D62758DF8592C333B62F696E8B760442126933744A6630E55C9A0D68C451F6E2845D557BB6662359B4B2E4
export superusers # required for old GRUB version (see: https://bugs.launchpad.net/ubuntu/+source/grub2/+bug/718670)
```

Don't forget to regenerate the GRUB configuration to apply the changes:
``` bash
# If available, use:
update-grub

# If not, use (adapt for your setup):
grub-mkconfig -o /boot/grub/grub.cfg
```

Note
: You may want to edit `/etc/grub.d/10_linux` to allow booting without a password. This effectively makes entries read-only. Search for the definition of the `CLASS=` variable, and append `--unrestricted` to it.
