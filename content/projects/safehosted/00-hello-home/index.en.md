---
title: "Hello Home"
date: "2021-09-30T15:53:36+02:00"
keywords: []

weight: 0

categories: [ "selfhosting", "nas", "webhosting" ]
tags: []

draft: false
---
# Hello Home

So you're looking into building your own home server.

Well, that's a fun idea, I can see why you would want to do that. Maybe you'd like to create as safe space to store all your data while not relying on a third-party cloud service? Maybe you'd like to host your own cloud services, or a Minecraft server, or a personal blog, or all of those at the same time? Or maybe you just see it as an opportunity to make your life easier while learning new stuff on the way?

Well, no matter what your reason is, I think it's a good idea.

I may even be able to help you with this project, since I've already built one myself, and I may know a thing or two that you could find useful. It's definitely a long journey with a lot of pitfalls, especially if you're just starting out with Linux or networking in general. I've acquired some experience while doing mine and solved many problems I could now help you avoid, and I'd be happy to do so.

You know what? Let's do it.

I'm starting a new series about just that. I'm going to log everything I did from the very beginning of my personal home server project, from selecting hardware to deploying software, as well as upgrading and maintaining everything in good shape. I hope you'll be able to learn from my mistakes, and successfully build the server of your dreams in no time.
