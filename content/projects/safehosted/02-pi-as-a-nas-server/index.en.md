---
title: "A Raspberry Pi as a NAS server"
date: "2021-10-02T19:36:01+02:00"
keywords: []

weight: 0

categories: [ "selfhosting" , "nas" ]
tags: [ "raspberry-pi", "partitioning", "file-system", "samba" ]

draft: false
---
# A Raspberry Pi as a NAS server

When I was finished playing with Pi-hole and wrecking my entire home setup in the process, I attempted to setup the Pi as a NAS server. I figured it would be best to start small like this and add functionalities along the way.

On a fresh install, since it's a server, the first thing to do is to disable the desktop environment using the `raspi-config` command, in order to save resources and to keep parasitic features to a minimum. For example, there is a service part of the desktop environment that will automatically mount USB devices under the `/run` directory, and I don't want it to interfere with the mounting commands I'll be issuing later. In parallel, I got my hands on an unused 2TB internal hard-drive that I scraped from an old computer, and which should fit perfectly as a big storage device.


## Adding storage

The first step is to figure out how to attach this drive to the Pi. I ended up buying an external case to transform this internal hard-drive into an external one, which I can then connect to the Pi via USB. I know that using USB will be a considerable bottleneck to transfer speed, but that is the only easy way I can think of to attach the drive to the Pi, since it has limited IO capabilities. Also, the drive needs more power than what a standard USB port can deliver, so it must be plugged in with his own power adapter, and having two outlets occupied for one system is a bit frustrating.


### Getting the drive's device file

Once I got the drive connected, it was time to setup it up as storage. First thing to do is to identify which file was assigned to the drive by the system. In the Linux world, everything is made accessible as a file, and storage drives are no exception. When the drive is plugged in, the system creates a file under `/dev` which I can use to communicate with it, abstracting away the physical complexity. You can learn more about this behavior [here on Wikipedia](https://en.wikipedia.org/wiki/Everything_is_a_file).

```
$ lsblk
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda           8:0    0   1.8T  0 disk 
└─sda1        8:1    0   1.8T  0 part 
mmcblk0     179:0    0  14.9G  0 disk 
├─mmcblk0p1 179:1    0   256M  0 part /boot
└─mmcblk0p2 179:2    0  14.6G  0 part /
```

Using the `lsblk` command to list block devices, I can see two entries of type `disk`: `mmcblk0` is the microSD card on which the system is installed, and `sda` is the SATA drive I just plugged in. Other entries of type `part` represent how each disk is partitioned.

Disk partitioning

:   A disk is a physical unit, it's a thing you can hold in your hands and accidentally drop on the floor. Partitions are a way to logically split the disk into multiple sub-disks treated in isolation from each other. Partitions are defined in a partition table, which is stored in the very first [sector](https://en.wikipedia.org/wiki/Disk_sector) of the disk.


### Partitioning the disk

The next step is to partition the disk. In theory, there's nothing to do here since it is already partitioned with one big partition taking up all the available space. For good mesure though, I'm still going to override this partition to make sure it's compatible with the Pi. I'll use a tool called `fdisk` which can alter the partition table of the drive. Running `fdisk /dev/sda` starts an interactive shell in which I can make changes and review everything before definitely altering the partition table. Keep in mind that this step may result in all data present on the disk being unreadable depending on what you do, so use this with caution and make backups.

Entering the `g` command will create a new empty partition table of type GPT. Using the `n` command, I create a big partition taking all the available space. With the `t` command, I make sure that the partition type is set to `Linux filesystem`. I then check with the `p` command that everything is in order, and since I'm feeling overly confident I use the `w` command to write changes to the disk and irreversibly alter the partition table.

Partition tables types

:   Partitioning is a bit of a mess since it's quite complex and requires a high level of compatibility between systems. With reasonable simplifications, we can say that there exists two standards to represent partition tables: MBR and GPT. Defining partition tables in the **M**aster **B**oot **R**ecord format was the way to go years ago when disks were small, but this method is slowly being superseded by the **G**UID **P**artition **T**able standard.


### Installing a file system

Now that the disk is suitably partitioned, I must install a file system on the partition to be able to store data on it. File systems are used to abstract the physical arrangement of data on the disk. They provide an interface to manipulate files and folders that the system can use without caring about how bytes are stored on the disk. I'll use the `ext4` file system since it's the most suited with Linux systems, but I could opt for `exFAT` and make this disk compatible with both Linux and Windows machines. Though `exFAT` being of the `FAT` family, it needs a lot of memory to store it's index table, and I did not do the math but I'm not sure that the Pi has enough memory for a 2TB drive. You can learn more about why `FAT` file systems are memory hungry [here on Wikipedia](https://en.wikipedia.org/wiki/File_Allocation_Table).

The partition is also made available by the system as a file under the `/dev` directory, and running `lsblk` again tells me that the file associated with the partition is `/dev/sda1`. So to create the file system, I run the `mkfs.ext4 /dev/sda1` command. This operation will completely destroy any data that was present on the disk, since all [inodes](https://en.wikipedia.org/wiki/Inode) will be deleted, so if it was not already the case before partitioning, **make a backup before running this command**.


### Automatically mounting the disk

When the formatting process is done, the disk is ready to be mounted and used. As a mount point, I usually use directories under `/mnt` since it exists for this purpose, but feel free to use somewhere else if you desire, it should be safe anywhere. I'll simply run `mkdir /mnt/samba` to create the mount point, and run `mount /dev/sda1 /mnt/samba` to mount the drive at this location. No error message, everything is in order, the drive is now usable.

Using the `mount` command is not persistent across reboots however, so I need to tell the system to run it every time on startup. Luckily, that is exactly what the `/etc/fstab` file is for. For the system to be usable, some volumes must be mounted automatically at boot time like the one containing the root file system `/`. So `/etc/fstab` should already contain some entries similar to this:

```
$ cat /etc/fstab
proc                  /proc           proc    defaults          0       0
PARTUUID=7b09933d-01  /boot           vfat    defaults          0       2
PARTUUID=7b09933d-02  /               ext4    defaults,noatime  0       1
```

The line starting with `PARTUUID=7b09933d-02` corresponds to the partition on which the system is installed, and the rest of the line contains instructions about where and how to mount it. Here, the partition is referred by it's UUID, but `/etc/fstab` allows us to use other methods like device files for exemple. However, device files are not unique and can change in certain circumstances, so it's recommended to use UUIDs whenever possible. Getting the UUID of a partition is a simple as looking for the matching symbolic link under `/dev/disk/by-partuuid`.

```
$ ls -l /dev/disk/by-partuuid
total 0
lrwxrwxrwx 1 root root 15 May  7 16:13 7b09933d-01 -> ../../mmcblk0p1
lrwxrwxrwx 1 root root 15 May  7 16:13 7b09933d-02 -> ../../mmcblk0p2
lrwxrwxrwx 1 root root 10 Oct  2 19:53 911ca890-262b-4111-97aa-4d836d43f478 -> ../../sda1
```

Symbolic links

:   Symbolic links are files which point to another file. You can see it as a shortcut or an alias name for the pointed file. Deleting the link will not delete the pointed file, but editing the link will edit the target file.

Now that I have the partition UUID, I'll add a new entry to `/etc/fstab` to automatically mount this partition on startup.

```
$ cat /etc/fstab
proc                                            /proc           proc    defaults            0   0
PARTUUID=7b09933d-01                            /boot           vfat    defaults            0   2
PARTUUID=7b09933d-02                            /               ext4    defaults,noatime    0   1
PARTUUID=911ca890-262b-4111-97aa-4d836d43f478   /mnt/samba      ext4    defaults            0   2
```

You can find out what all these arguments mean by looking into Linux's man pages, in particular [man&nbsp;fstab.5](https://man7.org/linux/man-pages/man5/fstab.5.html).

To test things out, I'll first unmount the partition I manually mounted before by running `umount /mnt/samba`, and then run `mount -a` to instruct the system to read `/etc/fstab` and mount devices accordingly like it would during startup. Running `lsblk` again shows that the partition was mounted as expected.

```
$ lsblk
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda           8:0    0   1.8T  0 disk 
└─sda1        8:1    0   1.8T  0 part /mnt/samba
mmcblk0     179:0    0  14.9G  0 disk 
├─mmcblk0p1 179:1    0   256M  0 part /boot
└─mmcblk0p2 179:2    0  14.6G  0 part /
```


## Setting up Samba

Samba is a free and open-source piece of software that can share a directory over a network. It offers compatibility with both Windows and Linux clients.

The nicest thing I can say about it is that it's a huge mess, but it's almost the best solution available today for sharing directories between varied clients over a network. Alternatives exists like NFS - which may even outperform Samba in terms of performance - but it offers very limited compatibility with clients other than Linux machines and is less flexible in general.


### Installation

Installing Samba is as straightforward as running `apt install samba`, which will in turn create a sample configuration file `/etc/samba/smb.conf` and enable/start both `nmbd.service` and `smbd.service` services since Raspberry Pi OS uses systemd. For security concerns, I'll immediately stop and disable the first one, and if you think that letting it be is not that big of a deal, I highly suggest that you educate yourself about the [WannaCry](https://en.wikipedia.org/wiki/WannaCry_ransomware_attack) ransomware which made headlines in May 2017.

systemd

:   systemd is a software suit that is responsible, among other things, for managing services (aka daemons) on a wide variety of Linux distributions. A service is simply a program that runs in the background and does stuff for you. Enabling a systemd service means that this service will be launched on system startup. Configuration of unit files, *i.e.* files which describe a service and end with the `.service` extension, is done through the `systemctl` command.

```
$ systemctl stop nmbd
$ systemctl disable nmbd
Synchronizing state of nmbd.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install disable nmbd
Removed /etc/systemd/system/multi-user.target.wants/nmbd.service.
```


### Configuration

Samba configuration options are confusing at best, and infuriating at worse. The manpage [man&nbsp;smb.conf.5](https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html) contains over 7000 lines of options, a lot of which are simply aliases for compatibility and also to make the language feel more "natural". The package even comes with a dedicated tool called `testparm` to check if the configuration is correct and error-free. Needless to say, it's best to disable as many things as possible if you care about your mental health. Some noteworthy options are listed below:

- `disable netbios = yes`: make sure Samba does not respond to NetBIOS queries and does not bind to ports 137 through 139.

- `server min protocol = smb2`: make sure SMBv1 is disabled, buried, and forgotten about.

- `bind interfaces only = yes` and `interfaces = eth0`: make sure Samba is served only on specific interfaces.

- `[home]` section: by default, Samba will allow authenticated users to access their home directory. You may want to disable this behavior by commenting out the section entirely.


#### Setting up a "share"

Samba directories are configured in "shares". "Shares" can be seen as meta-directories, since they're used to provide compatibility between various clients file systems. A "share" is defined by a section whose name is the "share" name, and options describe how it should behave. A very basic "share" called `myshare` can be defined like the following:

```ini
[myshare]
    path = /mnt/samba/myshare
    read only = no
```

Only authenticated users will be able to use this share, and the content of `/mnt/samba/myshare` will be readable and writable. This is the configuration I'll use for now, but note that it can get quite a lot more complicated, with per-user "shares" and other cool possibilities like that.

Once I've added this snippet of config at the end of Samba's config file, I run `testparm` to check if there are any errors, and since everything is in order, I restart `smbd.service` to reload the configuration. I also need to create the `/mnt/samba/myshare` directory.

```
$ mkdir /mnt/samba/myshare
$ systemctl restart smbd
```


#### Users and permissions

Samba users are distinct from system users, and can be pulled from different sources like an LDAP directory or a MySQL database. However, a local Unix user with the same name must exist on the system for Samba to work. This is because of a permission problem.

Since Samba abstracts away the reality of the shared directory, it must at some point map remote users with local users to be able to validate file permissions. To add a file for example, a mounted "share" on your machine needs to be writable locally by you, but also by the local user on the remote server. This **will** cause you some headaches when debugging, as even the local `root` user won't be able to edit a remote file if it is mapped to an unprivileged user on the server, and getting `Permission denied` as `root` can be quite disturbing... In theory, it's possible to have three distinct users in the chain: the local user on the client machine, the Samba user in the database, and the local user on the remote server. And for you to be able to use this share, permissions are bound by the strictest link in the chain.

Here, I'll create a Samba user backed by the already existing `pi` Unix user. The default user backend is a local one called `tdbsam` and no further configuration is required to use it. User management is done via the `smbpasswd` command:

```
$ smbpasswd -a pi
New SMB password:
Retype new SMB password:
Added user pi.
```

Samba will also try its best to keep the Samba user's password in sync with the matching Unix user's one. Again, don't question it, and save yourself an afternoon of debugging by inputting the same password for both accounts.

Now, I'll make sure that `/mnt/samba/myshare` is both readable and writable by `pi` by running the following commands:

```
$ chown pi:pi /mnt/samba/myshare
$ chmod 700 /mnt/samba/myshare
```


## Connecting to the NAS

Now that everything is setup, I should be able to access the "share" with the appropriate client tool on my machines. But before that, I need to know the IP address of the Pi so I can connect to it. Running the `ip` command does the trick, and informs me that I should input `10.1.1.100` wherever needed.

The typical syntax to describe a Samba "share" looks like this:

```
//10.1.1.100/myshare
```

Replace every `/` with a `\` on Windows.


### Windows

Accessing the "share" with a Windows machine is by far the easiest method and can be done either by [mapping a network drive](https://support.microsoft.com/en-us/windows/map-a-network-drive-in-windows-10-29ce55d1-34e3-a7e2-4801-131475f9557d), or by [adding a network location](https://it.nmu.edu/docs/adding-network-location-windows), both of which happen in Windows' file explorer.


### Linux

Your favorite file explorer surely has a way to access SMB/CIFS "shares" using some variant of the `smbclient` tool. But the way I like to do it is to mount the "share" like any other drive, using the `mount -t cifs` command. It's usually the most performant solution since mounting has a very low overhead compared to user-level tools. You may need to install your distribution's variant of the `cifs-utils` package for this to work.

```
$ mkdir /mnt/myshare
$ mount -t cifs -o username=pi,uid=user //10.1.1.100/myshare /mnt/myshare 
```

Permissions are a bit wonky and should be specified manually. The system will mount the "share" as the local `root` user if the `uid=` option is not specified. It's also possible to set the password using the `password=` option, but it's better to ommit it when not in a script so it does not appear in clear text in your command history.