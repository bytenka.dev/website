---
title: "A Raspberry Pi as a router"
date: "2021-07-20T11:53:10+02:00"
keywords: []

weight: 0

categories: []
tags: []

draft: true
---
# A Raspberry Pi as a router

This part may seem unrelated to building a home server at first, but creating the network infrastructure that supports your server is actually a key point.
