---
title: "Raspberries everywhere"
date: "2021-10-01T17:51:20+02:00"
keywords: []

weight: 0

categories: [ "selfhosting", "nas" ]
tags: [ "raspberry-pi", "pi-hole" ]

draft: false
---
# Raspberries everywhere

Buying hardware is a challenge when you're starting out, especially if you're unsure about what you're trying to achieve. When I became more and more interested in running my own server, I first looked into pre-built all-in-one plug-and-play no-headaches NAS solutions from brands like Synology or QNAP. But since they are quite expensive on their own and that I wanted to use it to host other services as well, I had to find something more versatile. This is where the Raspberry Pi comes in.

NAS

:   **N**etwork **A**ttached **S**torage

    Files and folders can be made accessible over a network, meaning they are stored on a machine somewhere and your computer accesses this machine though the network. It's very useful when you want to share files between devices, or store them in a centralized and organized way.

A [Raspberry Pi](https://www.raspberrypi.org/) is a small and inexpensive computer which can do basically anything that does not require huge amounts of computing power. They're typically used as a learning and prototyping platform, as well as an alternative to Arduino boards when an fully fledged operating system is required. They usually run a custom version of the Debian Linux distribution called Raspberry Pi OS (formerly known as Raspbian), but you can run any distribution you like if you're motivated enough.

Being a computer with an operating system, you can configure it to run any program you want and even use it as a daily driver if you like painful experiences. A popular usage however, is to use it as a network-wide ad blocker and reverse DNS resolver by installing [Pi-hole](https://pi-hole.net/). This is how I started to use mine to familiarize myself with the device, and it's a great way to learn more about networking if you're interested in the field. This document is not about Pi-hole, but we will have the occasion to talk more about domain resolution in the future. 

{{%
  figure 
  src="img/pi.jpg"
  caption="Raspberry Pi 3 in it's typical red-white box, and a Raspberry Pi Zero"
%}}

Installing Raspberry Pi OS is made very easy thanks to the new [Raspberry Pi Imager](https://www.raspberrypi.org/software/) tool. Not that the previous method was hard by any means, but the installer provides a nice graphical interface and simply requires you to click on some buttons. The Pi uses a microSD card as non-volatile storage, meaning you have to install the software from your computer to the card and then just plug it in. If you've already used Linux-based systems before, you can jump into a terminal and configure your Pi right away. But if it's your first experience, I'd recommend that you take some time to familiarize yourself with the OS and always make sure you know what you're doing.