---
title: "À propos"
type: "about"

draft: true
---
# À propos

Bienvenue sur mon blog.

Je m'appelle Hugo, et j'ai {{< age "1999-10-24" >}}.

Je suis étudiant à Toulouse, actuellement en deuxième année de master mention sécurité des systèmes d'information et des réseaux. J'espère décrocher mon diplôme fin 2022 si tout va bien, et j'envisage de m'arrêter là car je n'aime pas vraiment l'ambiance qui entoure le milieu universitaire au-delà.

Je suis en général passionné par tout ce que je fais, principalement parce que je tends à ignorer tout ce qui ne me passionne pas... Heureusement, il y a beaucoup de choses que je trouve intéressantes donc ça n'est pas tellement un problème :upside_down_face:. J'ai commencé en tant que féru de programmation avec des langages bas niveau comme le C ou le C++, mais mon intérêt a rapidement diminué lorsque j'ai commencé à étudier les langages de haut niveau et les technologies du web. Donc à la place, j'ai commencé à m'intéresser à l'administration des réseaux et des systèmes lorsque j'ai eu mon premier [Raspberry Pi](https://www.raspberrypi.org/), et je suis aujourd'hui un Linuxien épanoui et propagandiste acharné (évidemment, comment ne pas l'être :rolling_eyes:).

Quand je ne travaille pas pour moi, j'ai besoin de me sentir utile aux autres et à la société. Je rêve d'un monde sans compétition directe et dans lequel l'entraide serait une valeur fondamentale. Je considère que ma valeur en tant qu'individu est directement dépendante de la quantité de bonheur que je peux apporter aux autres autour de moi. Je suis également un perfectionniste, ce qui est un atout pour les relations sociales, mais un poids pour tout le reste honnêtement. Bien que je ne joue pas autant qu'avant, je suis un très grand amateur de jeux vidéo, en particulier ceux qui racontent une histoire. J'ai trop de jeux favoris pour les énumérer ici, même si [Celeste](http://www.celestegame.com/) occupe une place toute particulière dans mon cœur. J'aime recommander des jeux, donc je vais laisser l'algorithme décider d'un autre pour moi&nbsp;: le jeu favori du jour est {{% favgame %}}, jouez-y&nbsp;!

Je pense que c'est tout ce que je peux partager avec confiance en public. J'espère que vous trouverez des informations utiles sur ce blog, et n'hésitez pas à me contacter si vous avez besoin de quoi que ce soit, je suis toujours heureux de pouvoir aider.
