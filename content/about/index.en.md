---
title: "About"
type: "about"

draft: false
---
# About me

Welcome to my personal blog.

My name is Hugo, I'm {{< age "1999-10-24" >}} and I'm french. <sub>I know I know... I'm sorry ok?</sub>

I'm an IT student in Toulouse, currently enrolled in a master's degree about computer and network security. I expect to graduate by the end of 2022 if things go well, and I plan to stop there since I don't like the general vibe of the academic world beyond that.

I'm pretty passionate with everything I do, simply because I tend to completely ignore anything else I'm not passionate about... But luckily, I find a lot of things interesting so it's not that much of a problem :upside_down_face:. I started as a programming freak with low-level languages like C and C++, but quickly got bored with high-level ones when I learned about web technologies and how many Node.js modules were available. So instead, I turned to computer and network administration when I got my first [Raspberry Pi](https://www.raspberrypi.org/), and I'm now a full-time Linux enthusiast and propagandist (of course I am, how can you not :rolling_eyes:).

When I'm not working for myself, I need to feel useful to others and to society. I fancy a world without direct competition in which everyone cares about everyone else and everything. I consider that my value as an individual is directly tied to how much happiness I can bring to people around me. I'm also a perfectionist, which I consider to be a good thing socially and a bad thing for basically everything else. Despite not playing as much as before, I'm a huge fan of video games, especially story-driven ones. I have too many favorites to enumerate here, although [Celeste](http://www.celestegame.com/) has a very special place in my heart. I also like to recommend games so I'll let the algorithm decide of another one for me: today's favorite video game is {{% favgame %}}, go play it!

Anyway, I think that's everything I'm confortable with sharing publicly. I hope you can find useful information on this blog, and do not hesitate to contact me if you need anything, I'm always happy to help.